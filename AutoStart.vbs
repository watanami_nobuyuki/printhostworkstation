'*********************************************************************
' 　PrinHostWorkstation
'　端末設定自動印刷ツール
'　ホスト名が端末マスタに存在しないときはエラーで止まります。
'
'*********************************************************************

FileName = "PrintHostWorkstation.xlsm"
MacroName = "AutoPrintStart"


'現在のパス取得
Dim pwd
Set pwd = Wscript.CreateObject("Scripting.FileSystemObject")
FilePath = pwd.getParentFolderName(WScript.ScriptFullName)
'Excelインスタンス
Dim ExlObj
Set ExlObj = Wscript.CreateObject("Excel.Application")

ExlObj.visible=False
ExlObj.Workbooks.Open FilePath & "\" & FileName
ExlObj.Application.Run MacroName
